create table school(
	id int(11) PRIMARY KEY  AUTO_INCREMENT NOT NULL,  
	name varchar(150) NOT NULL, 
	address varchar(150) NOT NULL, 
	phone varchar(14) NOT NULL
);