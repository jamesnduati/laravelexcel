  <?php

public function fileimport()
    {
        Excel::filter('chunk')->selectSheetsByIndex(0)->load('collegec_database_digitalocean.xls')->chunk(100, function($results)
        {
            $success = $fail = "";
            $success_count = $fail_count = 0;

            foreach($results as $school) {
              $slug = str_slug($school->name);
              $url = 'schools/' . $slug;
              $logo = $slug . '.jpg';
              
              try {
                School::updateOrCreate(
                    ['id' => $school->id],
                    [
                      'id' => $school->id,
                      'name' => $school->name ?: '',
                      'describtion' => $school->describtion ?: '',
                      'url' => $url,
                      'location' => $school->location ?: '',
                      'fees' => $school->fees ?: '',
                      'logo' => $logo,
                      'undergradcourses' => $school->undergradcourses ?: '',
                      'postgradcourses' => $school->postgradcourses ?: '',
                      'shortcoursesandcertifications' => $school->shortcoursesandcertifications ?: '',
                      'technicalandvocationalcourses' => $school->technicalandvocationalcourses ?: '',
                      'website' => $school->website ?: '',
                      'phone' => $school->phone ?: '',
                      'email' => $school->email ?: ''
                    ]
                );
    
                $success .= "<li>" . $school->name . "</li>";
                $success_count++;
              } catch (\Exception $e) {
                $fail .= "<li>" . $school->name . "<br>" . $e->getMessage() . "</li>";
                $fail_count++;
              }
            }

            echo $success_count . " inputted successfully:<br><ul>" . $success . "</ul>";
            echo $fail_count . " not inputted due to errors:<br><ul>" . $fail . "</ul>";
        });
    }
