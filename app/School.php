<?php

namespace App;



use Illuminate\Database\Eloquent\Model;



class School extends Model

{

    
  	public $fillable = ['name','address','phone'];

}
